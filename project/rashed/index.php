<!DOCTYPE html>
<html lang="en">

<head>
  
  <!-- Meta Tag -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Personal Portfolio</title>
  
  <!-- Favicon -->
  <link rel="icon" href="images/favicon/favicon.jpg" type="image/gif" sizes="16x16"> 
  
  <!-- All CSS Plugins -->
  <link rel="stylesheet" type="text/css" href="css/plugin.css">
  
  <!-- Main CSS Stylesheet -->
  <link rel="stylesheet" type="text/css" href="css/style.css">
  
  <!-- Google Web Fonts  -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
  
  
  
</head>

  <body>
    
    
	
	<!-- Preloader Start -->
  <div class="preloader">
   <p>Loading...</p>
 </div>
     <!-- Preloader End -->

    
    
    <!-- Menu Section Start -->
    <header id="home">
        
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                
                    <div class="col-sm-3">
                        <div class="logo">
                            <a href="index.php">RASHED</a>
                        </div>
                    </div>
                    
                    <div class="col-sm-9">
                        <div class="navigation-menu">
                            <div class="navbar">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active"><a class="smoth-scroll" href="#home">Home <div class="ripple-wrapper"></div></a>
                                        </li>
                                        <li><a class="smoth-scroll" href="#experience">Experience</a>
                                        <li><a class="smoth-scroll" href="#about">About</a>
                                        </li>
                                        <li><a class="smoth-scroll" href="#portfolio">Portfolio</a>
                                        </li>
                                        </li>
                                        <li><a class="smoth-scroll" href="#services">services</a>
                                        </li>
                                        <li><a class="smoth-scroll" href="#contact">Contact</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </header>
     <!-- Menu Section End -->  
    
    
    <!-- Home Section Start -->
    <section class="home-section pattern-bg1">
        <div class="container">
            <div class="row">
                
                 <div class="col-sm-offset-2 col-md-4 col-sm-6 margin-left-setting">
                    <div class="margin-top-150">
                        
                     <div class="table-responsive">
					    <table class="table">
							<tr>
							    <td>Name</td>
								<td><?php echo $_POST['name']; ?></td>
							 </tr>
                             <tr>
								<td>Email</td>
								<td><?php echo $_POST['email']; ?></td>
							</tr>
							<tr>
								<td>Designation</td>
								<td><?php echo $_POST['designation']; ?></td>
							 </tr>
							<tr>
								<td>Experience</td>
								<td><?php echo $_POST['experiance']; ?></td>
							</tr>
							<tr>
								<td>Contact</td>
								<td><?php echo $_POST['contact']; ?></td>
							</tr>
                            <tr>
								<td>Resume</td>
								<td style="background-color: #f7639a;"><a href="resume/index.html" target="_blank" data-toggle="tooltip" data-placement="top" title="Check Out My Resume">Resume.pdf</a></td>
							</tr>
						</table>
					  </div>
                     </div>
                   </div>
                 
                 <div class="col-md-5 col-sm-6">
                    <div class="me-image">
                      <img src="images/bg/rashed.jpg" alt="">
                  </div>
                </div>
              </div>
            </div>
        </section>
        <!-- Home Section End -->
        
        
        
    <!-- Experience Start -->
    <section id="experience" class="section-space-padding">
        <div class="container">
           <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>My Experience.</h2>
                         <div class="divider dark">
						   <i class="icon-graduation"></i>
						  </div>
                        <p>I've got chops in all phases of the design process.</p>
                    </div>
                </div>
            </div>
            
            <div class="row">
            
            <div class="col-md-6 col-sm-6">
				<div class="experience">
				
				<div class="experience-item">
					<div class="experience-circle">
					   <i class="icon-graduation"></i> 
                       <p>8, Nov 2016</p>
					</div>
					<div class="experience-content experience-color-blue">
						<h4>Master Degree in Web Development</h4>
                        <h6>College Name here</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eleifend risus sed feugiat faucibus.</p>
					</div>
				 </div>
				
				<div class="experience-item">
					<div class="experience-circle">
						<i class="icon-trophy"></i> 
                        <p>8, Nov 2016</p>
					</div>
					<div class="experience-content experience-color-blue">
						<h4>Diploma in Engineering</h4>
            <h6>National Polytechnic Institute</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eleifend risus sed feugiat faucibus.</p>
					</div>
				 </div>
				
				<div class="experience-item">
					<div class="experience-circle">
						<i class="icon-book-open"></i> 
                        <p>8, Nov 2016</p>
					</div>
					<div class="experience-content experience-color-blue">
						<h4>Collage Education</h4>
            <h6>Chandpur Govt Collage</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eleifend risus sed feugiat faucibus.</p>
					</div>
				 </div>
				
			 </div>
			</div>

			<div class="col-md-6 col-sm-6">
				<div class="experience">
				
          <div class="experience-item">
           <div class="experience-circle experience-company pink-color-bg">
            <i class="icon-energy"></i>
          </div>
          <div class="experience-content">
            <h4>Web Application Development-PHP</h4>
            <h6> BASIS INSTITUTE OF TECHNOLOGY & MANAGEMENT(BITM) </h6>
            <p>BDBL Bhaban (Lavel-3), 12 Kawran Bazar, Dhaka-1215, Bangladesh training program held from 06-03-2017 to 04-06-2017</p>
          </div>
        </div>

				<div class="experience-item">
					<div class="experience-circle experience-company pink-color-bg">
						<i class="icon-ghost"></i>
					</div>
					<div class="experience-content">
						<h4>Web Design & Development</h4>
            <h6>Pondit.com</h6>
						<p> RH Home Center, Green Road, Tejgaon, Dhaka from 01-08-2017 to 30-02-2018. </p>
					</div>
				 </div>
				
				<div class="experience-item">
					<div class="experience-circle experience-company pink-color-bg">
						<i class="icon-compass"></i>
					</div>
					<div class="experience-content">
						<h4>WebRec System PVT. LTD.</h4>
             <h6>2005-2007, Senior Designer</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eleifend risus sed feugiat faucibus.</p>
					</div>
				 </div>
				
			 </div>
			</div>
            
           </div>
        </div>
    </section>
    <!-- Experience End -->
    
    
    <!-- About Start -->
    <section id="about" class="about section-space-padding pattern-bg">
       <div class="container">
          <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>About Me.</h2>
                         <div class="divider dark">
						   <i class="icon-emotsmile"></i>
						  </div>
                        <p>Look no more. I'm a web design & development specialist.</p>
                    </div>
                </div>
            </div>
            
            
            <div class="row">
              <div class="col-md-7">
               <div class="about-me-text margin-top-50">
                 <p> Hello! I'm Md. Rashed Ahmed and my home town is Chandpur, Bangladesh. Recently I have completed my Diploma in Engineering in Computer Department from NPI University of Bangladesh, Dhaka. HSC  from Chandpur Government Collage, Bangladesh. Now I am studying Bsc. <br/><br/>
                  I started web design and development after 2017 and have enjoyed working as a freelancer. I am skilled in XHTML, CSS, PHP-MySQL, Adobe Dreamweaver, Adobe Photoshop, Adobe Illustrator and a bit of javascript. I try to learn a new skill every day and I am so passionate about my work that I won't give up until I am fully satisfied with the work. I aspire better performance in my working field. The qualification that in going to achieve to the best of my consciousness will be effective to expose my exclusive performance Now I am currently available for freelance work.<br/><br/>
                  I have all the necessary web page design skills but I keep my web design prices extremely competitive.<br/>
                  If you would like to hire me please use the contact form to get in touch with me. You can also reach me via a phone Call or SMS to +88 01962-514572 or via Email at rashed.ahmed@engineer.com, or via Facebook or via Linkedin or follow me in Twitter or place an order straight up by clicking this link order now. I will get in touch with you as soon as possible!!! 
                </p>
                </div>
              </div>

              <div class="col-md-5">
                <div class="about-me  margin-top-50 margin-bottom-50">
                      <p style=" text-align: justify center; font-size: 16px;">Hello! I'm <Strong>Md. Rashed Ahmed </strong> and I'm a freelance <Strong>Web Designer &amp; Developer</Strong>.</p>
                    <img src="images/bg/profile.png" width="292" height="356">
                </div>
              </div>
            </div>
          </div>
       </section>
  
       <!-- About End -->
    

    
    <!-- Portfolio Start -->
    <section id="portfolio" class="portfolio section-space-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>My Portfolio.</h2>
                          <div class="divider dark">
						   <i class="icon-picture"></i>
						   </div>
                        <p>Take a look at some of my recent projects.</p>
                    </div>
                </div>
            </div>
            
            <div class="row">
              <div class="col-md-2">
                <ul class="portfolio">
                    <li class="filter" data-filter="all">all</li>
                    <li class="filter" data-filter=".apps">apps</li>
                    <li class="filter" data-filter=".mockups">Recent-Project</li>
                    <li class="filter" data-filter=".wordpress">wordpress</li>
                </ul>
              </div>
            
            <div class="col-md-10">
                <div class="portfolio-inner margin-top-30">
                
                
                    <div class="col-md-4 col-sm-6 col-xs-12 mix apps">
                        <div class="item">
                            <a href="images/portfolio/1.jpg" class="portfolio-popup" title="Students Management System">
                                <img src="images/portfolio/1.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 col-xs-12 mix mockups">
                        <div class="item">
                            <a href="images/portfolio/2.jpg" class="portfolio-popup" title="Consturction Company">
                                <img src="images/portfolio/2.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 col-xs-12 mix apps">
                        <div class="item">
                            <a href="images/portfolio/3.jpg" class="portfolio-popup" title="Minimal Template">
                                <img src="images/portfolio/3.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 col-xs-12 mix mockups wordpress">
                        <div class="item">
                            <a href="images/portfolio/4.jpg" class="portfolio-popup" title="Trading Template">
                                <img src="images/portfolio/4.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 col-xs-12 mix wordpress apps">
                        <div class="item">
                            <a href="images/portfolio/5.jpg" class="portfolio-popup" title="Dolore Sit Template">
                                <img src="images/portfolio/5.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 col-xs-12 mix apps mockups wordpress" title="Bangladesh Musician's Fundation (BMF)">
                        <div class="item">
                            <a href="images/portfolio/6.jpg" class="portfolio-popup">
                                <img src="images/portfolio/6.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
       </div>
        
        <div class="text-center margin-top-50">
          <a class="button button-style button-style-dark button-style-color-2 smoth-scroll" href="#contact">Hire Me!</a>
          </div>
     
    </section>
    <!-- Portfolio End -->
    
    
    
   
    
    
    
      <!-- statistics -->
      <section class="statistics-section section-space-padding bg-cover text-center">
         <div class="container">     

            <div class="row">

           <div class="col-md-3 col-sm-6 col-xs-6">
            <div class="statistics bg-color-1">
              <div class="statistics-icon"><i class="icon-mustache"></i>
              </div>
              <div class="statistics-content">
                <h5><span data-count="2025" class="statistics-count">2025</span></h5><span>Projects Done</span>
              </div>
            </div>
          </div>
          
          <div class="col-md-3 col-sm-6 col-xs-6">
            <div class="statistics bg-color-6">
              <div class="statistics-icon"><i class="icon-emotsmile"></i>
              </div>
              <div class="statistics-content">
                <h5> <span data-count="1200" class="statistics-count">1200</span></h5><span>Happy Clients</span>
              </div>
            </div>
          </div>
          
          <div class="col-md-3 col-sm-6 col-xs-6">
            <div class="statistics bg-color-4">
              <div class="statistics-icon"><i class="icon-hourglass"></i>
              </div>
              <div class="statistics-content">
                <h5><span data-count="8000" class="statistics-count">8000</span></h5><span>Hours of Work</span>
              </div>
            </div>
          </div>
          
          <div class="col-md-3 col-sm-6 col-xs-6">
            <div class="statistics bg-color-5">
              <div class="statistics-icon"><i class="icon-cup"></i>
              </div>
              <div class="statistics-content">
                <h5><span data-count="4000" class="statistics-count">4000</span></h5><span>Cup of Coffee</span>
              </div>
            </div>
            </div>

         </div>
       </div>
    </section>
    <!-- statistics end -->

    
    
    
    <!-- Services Start -->
    <section id="services" class="services-section section-space-padding">
        <div class="container">
           <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>My Services.</h2>
                          <div class="divider dark">
						   <i class="icon-drop"></i>
						 </div>
                        <p>Let's build together.</p>
                    </div>
                </div>
            </div>
            
            <div class="row margin-top-30">
            
            <div class="col-md-4 col-sm-6">
				<div class="services-detail">
					<div class="services-content">
            <i class="icon-screen-smartphone color-1"></i>
            <h3>Mobile Design</h3>
            <hr>     
          </div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<div class="services-detail">
          <i class="fa fa-code color-3"></i>
					<h3>PHP</h3>
					<hr>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<div class="services-detail">
					<i class="fa fa-code color-3"></i>
					<h3>Clean Code</h3>
					<hr>
				</div>
			</div>
            
            <div class="col-md-4 col-sm-6">
				<div class="services-detail">
					<i class="icon-support color-4"></i>
					<h3>Full Support</h3>
					<hr>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<div class="services-detail">
					<i class="fa fa-html5 color-5"></i>
					<h3>HTML5 Design</h3>
					<hr>
				</div>
			</div>

			<div class="col-md-4 col-sm-6">
				<div class="services-detail">
					<i class="icon-bulb color-6"></i>
					<h3>CSS3 Design</h3>
					<hr>
				</div>
			</div>
            
            </div>
        </div>
    </section>
    <!-- Services End -->
    
    
    
    <!-- Call to Action Start -->
    <section class="call-to-action bg-cover section-space-padding text-center">
       <div class="container">
         <div class="row">
           <div class="col-md-8">
             <h2>Do You Want to Know More About Me?</h2>
             </div>
             
            <div class="col-md-4">
             <div class="text-center">
               <a class="button button-style button-style-color-2 smoth-scroll" href="#contact">Contact Me</a>
            </div>
            
            </div>    
          </div>
         </div>
       </section>
       <!-- Call to Action End -->
       
       
       
       
    <!-- Contact Start -->
    <section id="contact" class="section-space-padding pattern-bg2">
       <div class="container">
          <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Contact Me.</h2>
                          <div class="divider dark">
						   <i class="icon-envelope-open"></i>
						 </div>
                        <p>Do You Want to Know More About Me? Please contace me..</p>
                    </div>
                </div>
            </div>
            
         
         <div class="margin-top-30 margin-bottom-50">
           <div class="row">
           
             <div class="col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8">   
                 
               <div class="row">
                 <div class="contact-us-detail"><a href="mailto:name@domain.com">name@domain.com</a></div>
                 <form action="store.php" method="post" id="cont-form" name="cont-form" class="contact-us pattern-bg">

                   <div class="col-sm-6">
                    <div class="form-group">
                      <input type="text" id="name" class="form-control" name="name" placeholder="Your Name" required="">
                    </div>
                  </div>

                  <div class="col-sm-6">
                   <div class="form-group">
                     <input type="email" id="email" name="email" class="form-control" placeholder="Your Email" required="">
                   </div>
                 </div>

                 <div class="col-sm-6">
                  <div class="form-group">
                   <input type="text" id="website" name="website" class="form-control" placeholder="Your Website">
                 </div>
               </div>

               <div class="col-sm-6">
                <div class="form-group">
                 <input type="text" id="address" name="address" class="form-control" placeholder="Where are You From?" required="">
               </div>
             </div>

             <div class="col-sm-12">
              <select id="subject" name="subject" class="form-group form-control" required="">
               <option value="" selected disabled>Subject</option>
               <option>Website Design & Development</option>
               <option>Wordpress Development</option>
               <option>Search Engine Optimization</option>
               <option>Mobile Website</option>
               <option>I Want to General Talk</option>
               <option>Other</option>
             </select>
           </div>

           <div class="col-sm-12">
             <div class="textarea-message form-group" >
               <textarea  id="message" name="message" class="textarea-message form-control" placeholder="Your Message" rows="5" required=""></textarea>
             </div>
           </div>


           <div class="text-center">      
             <button type="submit" class="button button-style button-style-dark button-style-color-2">Submit</button>
           </div>

         </form>
                   
                </div>
			  </div>
            </div>
           
        
        </div>
       </div>
     </section>
     <!-- Contact End -->
       
        
        
        
    <!-- Footer Start -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
               
            <div class="col-md-12">
              <ul class="social-icon margin-bottom-30">
                 <li><a href="https://www.facebook.com/Rashed.Ahmed.008" target="_blank" class="facebook"><i class="icon-social-facebook"></i></a></li>
                 <li><a href="#" target="_blank" class="twitter"><i class="icon-social-twitter"></i></a></li>
                 <li><a href="#" target="_blank" class="google-plus"><i class="icon-social-google"></i></a></li>
                 <li><a href="#" target="_blank" class="instagram"><i class="icon-social-instagram"></i></a></li>
                 <li><a href="#" target="_blank" class="dribbble"><i class="icon-social-dribbble"></i></a></li>
               </ul>
          </div>
              
             <div class="col-md-12 uipasta-credit">
                <p>Design By <a href="https://www.facebook.com/Rashed.Ahmed.008" target="_blank" title="Rashed">Rashed Ahmed</a></p>
                </div>
                
             </div>
        </div>
    </footer>
    <!-- Footer End -->
    
    
    <!-- Back to Top Start -->
    <a href="#" class="scroll-to-top"><i class="icon-arrow-up-circle"></i></a>
    <!-- Back to Top End -->
    
    
    <!-- All Javascript Plugins  -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/plugin.js"></script>
    
    <!-- Main Javascript File  -->
    <script type="text/javascript" src="js/scripts.js"></script>
  
  
  </body>
 </html>