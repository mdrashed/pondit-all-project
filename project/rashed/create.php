<!doctype html>
<html lang="en">
<head>
    <title>Hello, world!</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!--google-fonts -->
    <link rel="stylesheet" href="">
    <!-- font-awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- coustom stylesheet -->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<section id="form">
    <div class="container">
        <h1>Profile</h1>
        <div class="row">
            <div class="col-md-12">
                <form action="index.php" name="" method="post" id="profile-update">
                    <div class="form-group row">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-lg" id="name" name="name" placeholder="Enter your name" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control form-control-lg" id="email" name="email" placeholder="Enter your email">
                        </div>`
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Designation</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-lg" id="designation" name="designation" placeholder="Enter your designation">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Experiance</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-lg" id="experiance" name="experiance" placeholder="Enter your experiance">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Contact</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control form-control-lg" id="contact" name="contact" placeholder="Enter your phone number">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>



    </div>
</section>








<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="asset/js/bootstrap.min.js"></script>
</body>
</html>